from typing import NamedTuple


class Person(NamedTuple):
    name: str
    email: str | None
