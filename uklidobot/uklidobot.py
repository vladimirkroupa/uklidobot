import ast
from typing import Tuple, List

from decouple import config

from uklidobot.mailer import send_email
from uklidobot.gspread_client import connect
from uklidobot.model import Person
from uklidobot.sheets.key_ownership_group_sheet import KeyOwnershipGroupsWorkSheet
from uklidobot.sheets.person_sheet import PersonWorkSheet
from uklidobot.sheets.sheet_repository import RepositorySheet


GSHEETS_SERVICE_ACCOUNT = config('GSHEETS_SERVICE_ACCOUNT', cast=ast.literal_eval)
SMTP_PASSWORD = config('SMTP_PASSWORD')
ERROR_EMAIL = config('ERROR_EMAIL')
RECIPIENTS_OVERRIDE = config('RECIPIENTS_OVERRIDE', default=None)

PERSON_KEYS_SHEET_ID = '1E9TVhHrWORau9sISgL-9yoCsRdnklr3LYwTPz-FMJqQ'
PERSON_WORKSHEET_NAME = 'Lidi v BKP'
KEY_OWNERSHIP_WORKSHEET_NAME = 'Skupiny na klíče'
REPOSITORY_SHEET_ID = '1SRWKRO6Mqyibo3krMA_04V1DQs0lKCatsdJImm1XT9A'
REPOSITORY_WORKSHEET_NAME = 'repository'

LAST_GROUP_STORED_KEY = 'email poslan naposledy skupine'


def compose_email(current_group: str, current_group_members: List[Person],
                  next_group: str, next_group_member_names: List[str]) -> Tuple[str, str]:
    curr_group_members_nice = ', '.join([member.name for member in current_group_members])
    next_group_members_nice = ', '.join(next_group_member_names)

    subject = "Služba na úklid chodby"
    body = f'<p>Tento týden má službu <strong>skupina {current_group}</strong> ({curr_group_members_nice})</p>'
    body += f'<p>Příští týden bude mít službu skupina {next_group} ({next_group_members_nice})</p>'
    # body += f'<p>Tabulka na služby je <a href="{SHEET_LINK}">zde</a>.</p>'
    body += f'<p><em>Zapisujte úklidy do papíru na chodbě, děkuji všem</<em></p>'

    return subject, body


def send_error_email(e: Exception, password: str):
    recipients = ERROR_EMAIL
    subject = "Error sending uklidobot email"
    body = str(e)
    cc = []
    send_email(recipients, cc, subject, body, password)


def main():
    try:
        client = connect(GSHEETS_SERVICE_ACCOUNT)
        key_ownership_sheet = KeyOwnershipGroupsWorkSheet(client, PERSON_KEYS_SHEET_ID, KEY_OWNERSHIP_WORKSHEET_NAME)
        person_sheet = PersonWorkSheet(client, PERSON_KEYS_SHEET_ID, PERSON_WORKSHEET_NAME)
        sheet_repository = RepositorySheet(client, REPOSITORY_SHEET_ID, REPOSITORY_WORKSHEET_NAME)

        previous_group = sheet_repository.read_stored_value(LAST_GROUP_STORED_KEY)
        if not previous_group:
            # first run or the repository has been emptied
            # take fist group
            current_group = key_ownership_sheet.list_group_names()[0]
        else:
            current_group = key_ownership_sheet.following_group_name(previous_group)

        next_group = key_ownership_sheet.following_group_name(current_group)

        current_group_member_names = key_ownership_sheet.list_group_members(current_group)
        next_group_member_names = key_ownership_sheet.list_group_members(next_group)

        current_group_members = [person_sheet.find_person_by_name(name) for name in current_group_member_names]
        next_group_members = [person_sheet.find_person_by_name(name) for name in next_group_member_names]
    except Exception as ex:
        send_error_email(ex, SMTP_PASSWORD)
        raise ex
    else:
        print('previous group:', previous_group)
        print('current group:', current_group)
        print('next group:', next_group)
        print('current group members:', current_group_member_names)

        recipients = [member.email for member in current_group_members]
        cc = [member.email for member in next_group_members]

        if RECIPIENTS_OVERRIDE:
            recipients = [RECIPIENTS_OVERRIDE]
            cc = [RECIPIENTS_OVERRIDE]

        subject, body = compose_email(current_group, current_group_members, next_group, next_group_member_names)
        send_email(recipients, cc, subject, body, SMTP_PASSWORD)
        sheet_repository.store_value(LAST_GROUP_STORED_KEY, current_group)


if __name__ == "__main__":
    main()
