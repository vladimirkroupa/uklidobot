from multiprocessing.connection import Client
from typing import NamedTuple, TypedDict

import gspread


class GoogleCredentials(TypedDict):
    type: str
    project_id: str
    private_key_id: str
    private_key: str
    client_email: str
    client_id: str
    token_uri: str
    auth_provider_x509_cert_url: str
    client_x509_cert_url: str


def connect(account: GoogleCredentials) -> Client:
    return gspread.service_account_from_dict(account)
