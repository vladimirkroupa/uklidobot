import operator
from typing import List, Tuple

import gspread
import nltk

from uklidobot.model import Person

NAME_COL_NO = 1
HEADER_OFFSET = 1


class PersonNotFoundError(Exception):

    def __init__(self, query: str, possible_matches: List[str]):
        message = f'Could not find member {query}.'
        if possible_matches:
            message += f' Closest possible matches: {possible_matches}'
        super().__init__(message)
        self.query = query
        self.possible_matches = possible_matches


class PersonWorkSheet:

    def __init__(self, client: gspread.Client, sheet_id: str, person_worksheet_name: str):
        sheet = client.open_by_key(sheet_id)
        self.groups_ws = sheet.worksheet(person_worksheet_name)

    def find_person_by_name(self, person_name: str) -> Person:
        """
        Finds a person by name, or raises an error indicating the closest matches if person was not found.

        :param person_name: Name of the person to search for.
        :return: Person with given name.
        :raises PersonNotFoundError: When there is no person with given name.
        """
        names = self.groups_ws.col_values(NAME_COL_NO)[HEADER_OFFSET:]
        if person_name in names:
            # person was found
            row_no = names.index(person_name) + HEADER_OFFSET + 1
            row = self.groups_ws.row_values(row_no)
            name = row[0]
            email = row[1] if len(row) > 1 else None
            return Person(name, email)
        else:
            # person was not found, calculate edit distance (Levenshtein distance) for names
            dist_from_query = [nltk.edit_distance(person_name, name) for name in names]
            names_dists_ord: List[Tuple[str, int]] = sorted(zip(names, dist_from_query), key=operator.itemgetter(1))

            hottest_dist = names_dists_ord[0][1]
            cutoff_dist = hottest_dist + 1  # best candidate edit distance + 1

            hottest_candidates_ord_by_dist = [name_dist_pair[0] for name_dist_pair in names_dists_ord
                                              if name_dist_pair[1] <= cutoff_dist]
            raise PersonNotFoundError(person_name, hottest_candidates_ord_by_dist)
