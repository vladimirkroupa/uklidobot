import ast

import pytest
from decouple import config

from uklidobot.gspread_client import connect
from uklidobot.sheets.sheet_repository import RepositorySheet

GSHEETS_SERVICE_ACCOUNT = config('GSHEETS_SERVICE_ACCOUNT', cast=ast.literal_eval)

TEST_SHEET_ID = '14tU2bFZ0s-BUgHC7-KVoumNSgfgEFUrvqTKDmNyiKVo'
READ_TEST_WORKSHEET_NAME = 'TEST repository read'
WRITE_TEST_WORKSHEET_NAME = 'TEST repository write'

client = connect(GSHEETS_SERVICE_ACCOUNT)
read_repository_sheet = RepositorySheet(client, TEST_SHEET_ID, READ_TEST_WORKSHEET_NAME)
write_repository_sheet = RepositorySheet(client, TEST_SHEET_ID, WRITE_TEST_WORKSHEET_NAME)


def test_can_read_value():
    value = read_repository_sheet.read_stored_value('foo')
    assert value == 'bar'


def test_when_no_key_present_then_none():
    value = read_repository_sheet.read_stored_value('gooby')
    assert value is None


def test_when_only_key_then_err():
    with pytest.raises(Exception):
        read_repository_sheet.read_stored_value('bar')


def test_when_multiple_values_then_err():
    with pytest.raises(Exception):
        read_repository_sheet.read_stored_value('baz')


def test_when_empty_sheet_then_appended(empty_sheet):
    write_repository_sheet.store_value('spam', 'eggs')
    stored_value = write_repository_sheet.read_stored_value('spam')
    assert stored_value == 'eggs'


def test_when_key_exists_then_overwritten(sheet_with_value):
    write_repository_sheet.store_value('spam', 'qux')
    stored_value = write_repository_sheet.read_stored_value('spam')
    assert stored_value == 'qux'


def test_when_key_does_not_exist_then_appended(sheet_with_value):
    write_repository_sheet.store_value('foo', 'bar')
    stored_value = write_repository_sheet.read_stored_value('foo')
    assert stored_value == 'bar'


@pytest.fixture
def empty_sheet():
    write_sheet = client.open_by_key(TEST_SHEET_ID)
    ws = write_sheet.worksheet(WRITE_TEST_WORKSHEET_NAME)
    ws.clear()
    yield


@pytest.fixture
def sheet_with_value():
    write_sheet = client.open_by_key(TEST_SHEET_ID)
    ws = write_sheet.worksheet(WRITE_TEST_WORKSHEET_NAME)
    ws.clear()
    ws.update_cell(1, 1, 'spam')
    ws.update_cell(1, 2, 'eggs')
    yield
