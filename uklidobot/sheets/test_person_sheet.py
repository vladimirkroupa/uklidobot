import ast

import pytest
from decouple import config

from uklidobot.gspread_client import connect
from uklidobot.model import Person
from uklidobot.sheets.person_sheet import PersonNotFoundError, PersonWorkSheet

GSHEETS_SERVICE_ACCOUNT = config('GSHEETS_SERVICE_ACCOUNT', cast=ast.literal_eval)

TEST_SHEET_ID = '14tU2bFZ0s-BUgHC7-KVoumNSgfgEFUrvqTKDmNyiKVo'
TEST_WORKSHEET_NAME = 'TEST lidi'

client = connect(GSHEETS_SERVICE_ACCOUNT)
person_sheet = PersonWorkSheet(client, TEST_SHEET_ID, TEST_WORKSHEET_NAME)


def test_can_find_person_by_name():
    person = person_sheet.find_person_by_name('František')
    assert person == Person(name='František', email='franta@riseup.net')


def test_can_find_person_with_no_email():
    person = person_sheet.find_person_by_name('Bětka')
    assert person == Person(name='Bětka', email=None)


def test_when_person_does_not_exist_then_error_with_possible_matches():
    """Possible matches with edit distance less or equal to edit distance of the best candidate plus one"""
    with pytest.raises(PersonNotFoundError) as not_found:
        person_sheet.find_person_by_name('Jyndra')

    assert not_found.value.possible_matches == ['Jindra']
