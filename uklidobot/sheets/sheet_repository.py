import gspread

KEY_COL = 1
VALUE_COL = 2


class RepositorySheet:

    def __init__(self, client: gspread.Client, sheet_id: str, repository_worksheet_name: str):
        sheet = client.open_by_key(sheet_id)
        self.repository_ws = sheet.worksheet(repository_worksheet_name)

    def read_stored_value(self, key: str) -> str | None:
        keys = self.repository_ws.col_values(1)
        if key not in keys:
            return None
        key_row = keys.index(key) + 1
        row = self.repository_ws.row_values(key_row)
        if len(row) < 2 or len(row) > 2:
            raise Exception(f"Expected row with two columns, found: {len(row)} columns")
        return row[1]

    def store_value(self, key: str, value: str) -> None:
        keys = self.repository_ws.col_values(1)
        first_empty_row = len(keys) + 1
        key_row = keys.index(key) + 1 if key in keys else first_empty_row
        self.repository_ws.update_cell(key_row, KEY_COL, key)
        self.repository_ws.update_cell(key_row, VALUE_COL, value)
