from typing import List

import gspread


class KeyOwnershipGroupsWorkSheet:

    def __init__(self, client: gspread.Client, sheet_id: str, groups_worksheet_name: str):
        sheet = client.open_by_key(sheet_id)
        self.groups_ws = sheet.worksheet(groups_worksheet_name)

    def list_group_names(self) -> List[str]:
        """
        lists only the names of the groups (in preserved order)
        :return: list of all group names
        """
        group_col_vals = self.groups_ws.col_values(1)
        group_names = group_col_vals[1:]
        return group_names

    def following_group_name(self, group: str) -> str:
        """
        Given the group name, returns the name of the FOLLOWING group. If group is the last group,
        will return the name of the first group.
        param group name of group to search for
        :return: the name of the FOLLOWING group if PRECEDING group with given name exists
        """
        group_names = self.list_group_names()
        if group not in group_names:
            raise Exception(f"Could not find group '{group}' among groups {group_names}")

        group_ix = group_names.index(group)
        next_group_ix = group_ix + 1 if len(group_names) > group_ix + 1 else 0
        return group_names[next_group_ix]

    def list_group_members(self, group: str) -> List[str]:
        """
        param group name of group to search for
        :return: list of group members' name, if group with given name exists
        """
        group_names = self.list_group_names()
        if group not in group_names:
            raise Exception(f"Could not find group '{group}' among groups {group_names}")

        row_no = group_names.index(group) + 2  # gspread rows start from 1, + 1 to skip header
        group_row_vals = self.groups_ws.row_values(row_no)
        group_members = group_row_vals[2:]  # names start from col C, skip two cols
        return group_members
