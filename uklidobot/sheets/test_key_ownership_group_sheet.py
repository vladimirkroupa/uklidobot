import ast

import pytest
from decouple import config

from uklidobot.gspread_client import connect
from uklidobot.sheets.key_ownership_group_sheet import KeyOwnershipGroupsWorkSheet

GSHEETS_SERVICE_ACCOUNT = config('GSHEETS_SERVICE_ACCOUNT', cast=ast.literal_eval)

TEST_SHEET_ID = '14tU2bFZ0s-BUgHC7-KVoumNSgfgEFUrvqTKDmNyiKVo'
TEST_WORKSHEET_NAME = 'TEST skupiny'

client = connect(GSHEETS_SERVICE_ACCOUNT)
key_ownership_sheet = KeyOwnershipGroupsWorkSheet(client, TEST_SHEET_ID, TEST_WORKSHEET_NAME)


def test_can_read_all_group_names():
    group_names = key_ownership_sheet.list_group_names()
    assert group_names == ['Vinohrady', 'Palmovka', 'Modřany']


def test_can_list_members_of_existing_nonempty_group():
    member_names = key_ownership_sheet.list_group_members('Vinohrady')
    assert member_names == ['franta', 'kamila', 'martin']


def test_can_get_following_group_of_nontrailing_group():
    group_name = key_ownership_sheet.following_group_name('Vinohrady')
    assert group_name == 'Palmovka'


def test_following_group_of_trailing_group_is_first_group():
    member_names = key_ownership_sheet.following_group_name('Modřany')
    assert member_names == 'Vinohrady'


def test_exception_when_following_group_of_nonexisting_group():
    with pytest.raises(Exception):
        key_ownership_sheet.following_group_name('Žižkov')
